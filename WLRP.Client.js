window.WLRP 				= window.WLRP 				|| {};
window.WLRP.forceScripts 	= window.WLRP.forceScripts 	|| false;

window.WLRP.init = function(element, component) {

	if (!window.jQuery) {

		throw new Error('WLRP requires jQuery. Please load the jQuery library before calling WLRP.init()!');

	} else {

		this.element 	= element;
		this.component 	= component;

		this.loadContent(this.element, this.component);
	}
	
}

window.WLRP.parseScripts = function(container) {
	$(container).find('script').each(function() {
		var js = $(this).text();
		eval(js);
	});
}

window.WLRP.loadContent = function(container, component) {

	var resourceToLoad = 'http://api.wlrp.eu/components/'+component+'/index.html';

	$(container).load(resourceToLoad, function() {
		if (window.WLRP.forceScripts) {
			window.WLRP.parseScripts(container);
		}
	});
}