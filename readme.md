# WLRP API Client

## Setup

Simply include the `WLRP.Client.js` library after including jQuery, then call `WLRP.init()` with your components name and container.

#### WLRP.init(container, component)
###### `container`
A jQuery selector string, jQuery object or DOM element to append the loaded component to.
###### `component`
The name of the component as hosted on `api.wlrp.eu/components/`. The component must consist of a single file, `index.html`, which should contain all of its dependencies.

## Full example
```
<div id="target"></div>
<script src="path/to/jquery.js"></script>
<script src="path/to/WLRP.Client.js"></script>
<script>
	WLRP.init('#target', 'example');

	/*

		This will load the contents of http://api.wlrp.eu/components/example/index.html into the element with ID 'target'.

	*/
</script>

```